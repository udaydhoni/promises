let p = new Promise ((resolve,reject)=>{
    resolve("Promise p is resolved");
})
let q = new Promise ((resolve,reject)=>{
    resolve("Promise q is resolved");
})

let r = new Promise ((resolve,reject)=>{
    resolve("Promise r is resolved");
})

let s = new Promise((resolve,reject)=>{
   reject("Promise s is rejected");
})

function all(promises) {
    return new Promise((resolve,reject)=>{
        let arr = [];
        let errorExists = false;
        new Promise ((resolve,reject)=>{
            promises.forEach((promise)=>{
                promise.then((result)=>{arr.push(result)},(err)=>{errorExists = true})
            })
            resolve();
        }).then(()=>{
            if (errorExists) {
                reject(new Error("error"))
            }
            if (errorExists == false) {
                resolve(arr)
            }
        })
    })
}

all([p,r,q]).then((result)=>{console.log(result)},(err)=>{console.log(err)});