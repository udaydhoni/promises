let p = new Promise((resolve,reject)=>{
    reject("Rejected Promise!")
})

p.catch((err)=>{
    console.log(err);
}) 
.finally(()=>{
    console.log("Promise settled");
})