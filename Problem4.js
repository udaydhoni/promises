console.log('A');

// Asynchronous code finises in 0 seconds (Callback Queue)

Promise.resolve().then(()=>console.log('B'))


// A promise that resolves right away (Microtask Queue)
Promise.resolve().then(() => console.log('C'));



console.log('D');