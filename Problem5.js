var firstPromise;
var secondPromise;

secondPromise = new Promise ((resolve,reject)=>{
    resolve("Second!")
})

firstPromise = new Promise ((resolve,reject)=>{
    resolve(secondPromise)
})

firstPromise.then((x)=>{
    return x
})
.then((y)=>{
    console.log(y)
})

