let x = fetch ('https://raw.githubusercontent.com/nnnkit/json-data-collections/masterrr/got-houses.json');

x.then((response)=>{
    if (response.status>=400) {
        return response.json().then((errResponseData)=> {
            const err = new Error ("Connection is fine but something was wrong")
            err.data = errResponseData;
            throw err;
        })
        
    } else {
        return response.json();
    }
})
.then((result)=>{console.log(result)})
.catch((err)=>{console.log(err)})